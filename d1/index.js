//goal is to create a server-side app using express web framework


//create an app using express
let express = require("express");

//identify a location or address where the connection will be created

let port = 4000;

//objective is to establish a connection
//express() -> this creates an express app
//application is the server

let application = express();

//assign/bind the connection to designated desired address or port
	//listen() => allows to create a listener to specified port or location
//syntax: serverName.listen(port, callback)
	//port => identifies the port/location where to execute the listener
	//callback => specifies a function or method to be executed when listener has been added or appended
	//using callback, included a response to the user to verify the connection has been properly established on the desired port

application.listen(port, () => {
	console.log(`Welcome to Express API Server! on port ${port}`)

});

//create a respponse in the main entry point of the server
	//create request that will display message at the base URI (`/`) of the server
	//when usin express, to create request method of GET, use get()
		//syntax - server.get(URI/path, callback/method)
			//URI/path - describes the designated path/location or route where request will be sent
			//callback - allows to identify the process on how the client and server would interact w each other
	application.get(`/`, (req, res)=>{
		//will describe how the server would interact/respond back to client
			//would need to transmit a message back to the client
			//send() - will allow to transmit data over a network
			res.send(`Greetings, welcome to course booking of MARVIN MOSQUERA`);

	});

	//setuo an environment in postman client app to simplify collection methods
		//1. new task is to integrate an environment variable in postman workspace to make work load when testing API collection easier.

/*console.log(`
Welcome to Express API Server
────────────█───────────────█
────────────██─────────────██
─────────────███████████████
────────────█████████████████
───────────███████████████████
──────────████──█████████──████
─────────███████████████████████
────────█████████████████████████
────────█████████████████████████
───███──▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒──███
──█████─█████████████████████████─█████
──█████─████████████████──███████─█████
──█████─██████────────█──█────███─█████
──█████─█████─▓▓▓▓▓▓▓█──█▓▓─▓─███─█████
──█████─███─█─▓▓▓▓▓▓█──█▓▓─▓▓─███─█████
──█████─██──█─▓▓▓▓▓█──█▓▓─▓▓▓─███─█████
──█████─███─█─▓▓▓▓█──█▓▓─▓▓▓▓─███─█████
──█████─█████────█──█─────────███─█████
──█████─█████████──██████████████─█████
───███──████████──███████████████──███
────────█████████████████████████
─────────███████████████████████
──────────█████████████████████
─────────────██████───██████
─────────────██████───██████
─────────────██████───██████
─────────────██████───██████
──────────────████─────████
`);*/