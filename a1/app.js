let server = require("express");

let port = 4000;

let application = server();

//to receive data from server body
application.use(server.json());

let to_do = [];

application.listen(port, () => {
	console.log(`Server ${port} is now running`);
});

//GET

application.get(`/list`, (req, res)=>{
	if (to_do.length > 0) {
		/*res.send(`The collection is not empty: ${JSON.stringify(to_do.length)} count(s)`);*/
		res.send(to_do);
	} 
	else {
		res.send(`The collection is empty`);
	}	
});

//POST

application.post(`/list`, (req, res)=>{
	let newTask = req.body;
	if(req.body.name !== "" && req.body.status !== ""){
		if(req.body.name === to_do.name){
			res.send(`No duplication of record`);
		}
		else{
			res.send(`Record ${req.body.name} added`);
			to_do.push(newTask);
		}
	}
	else{
		res.send(`All fields required`);
	}
});

//PUT

application.put(`/list`, (req, res)=>{
	if (to_do.length > 0) {
		for(let i = 0; i < to_do.length; i++){
			if (req.body.name === to_do[i].name) {
				res.send(`Record ${req.body.name} updated`);
				to_do[i].status = req.body.status;
			} 
			else {
				res.send(`No record found`);
			}
		}
	} 
	else {
		res.send(`Collection is empty`);
	}
});

//DELETE

application.delete(`/list`, (req, res)=>{
	if (to_do.length > 0) {
		for(let i = 0; i < to_do.length; i++){
			if (req.body.name === to_do[i].name) {
				res.send(`Record ${req.body.name} deleted`);
				to_do.splice(i, 1);
			} 
			else {
				res.send(`No record found`);
			}
		}
	} 
	else {
		res.send(`Collection is empty`);
	}
});